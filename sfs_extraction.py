#!/usr/bin/python

###
###	sfs_extraction.py
###	Reads output from SFS.pl (Cartwright and Ross-Ibarra 2011) and saves 
###	the loci names and the site frequency spectra.
###

import sys

#	Opens the file for reading, extracts the lines 3, 7, 10, 14, 17, 21, 24, ...
#		Lines 3, 10, 17, 24, ... Have loci names
#		Lines 7, 14, 21, 28, ... Have SFS information
def get_lines(sfs_output):
	#	Initialize lists to contain our information
	contents=list()
	loci_names=list()
	sfs=list()
	
	#	Open our file readonly
	with open(sfs_output, 'r') as f:
		#	Break it up by line, save it in a huge list
		for line in f:
			#	Use str.strip() to remove the newline characters at the end
			contents.append(str.strip(line))
		
		#	Now we can work on the list
		#	enumerate() allows us to access the list's items and indicies at
		#	the same time.
		for index, item in enumerate(contents):
			#	We add 1 here since lists begin counting from 0
			#	and we would like to begin counting from 1
			#	We modulus 7 since SFS info appears on every seventh line
			if (index+1)%7==0:
				sfs.append(contents[index])
				#	And the loci names always appear 4 lines behind the SFS
				loci_names.append(contents[index-4])
		#	And we return these lists as a tuple
		return(loci_names, sfs)

#	Takes two lists, prints the first and all but the first item of the other
def print_sfs(loci, sfs):
	#	If the lengths aren't the same, then exit
	if len(loci) != len(sfs):
		exit("You don't have the same number of loci and SFS!")
	#	Now we loop through one of the lists and get the relevant info
	for index in range(len(loci)):
		#	We make a temporary string to combine info from the two lists
		#	We take [1:] to remove the leading 0 0, since it is irrelevant
		tempstring=loci[index] + sfs[index][1:]
		print tempstring

#	Prints the usage info
def print_usage():
	print(
"""USAGE:
	sfs_extraction.py <SFS output>

where <SFS output> is a file created by SFS.pl (Cartwright, Ross-Ibarra 2011).
This script prints loci names and relevant SFS info to stdout.""")
	exit()

#	We check number of args
#	Two arguments in the list, since sys.arv[0] is the script that is running
if len(sys.argv) != 2:
	print_usage()
	exit()
#	We check to make sure the file exists and has read permissions
#	If not, exit gracefully
try:
	sfsfile=open(sys.argv[1], 'r')
except IOError:
	print "Your SFS output file is not readable, or does not exist!"
	exit()

#	Here is where we actually run the script
combined_info=get_lines(sys.argv[1])
loci=combined_info[0]
sfs=combined_info[1]
print_sfs(loci, sfs)