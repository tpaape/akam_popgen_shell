#shell script for making non-synonymous and synonymous site polymorphism tables for each gene alignment
for file in `ls -1 /srv/kenlab/Tim/libsequence/Ahal_fasta/`
do
    polydNdS -i /srv/kenlab/Tim/libsequence/Ahal_fasta/$file -P -O 26 

mv *.all_silent /srv/kenlab/Tim/libsequence/sildir

mv *.replacement  /srv/kenlab/Tim/libsequence/repdir
 
remove extra files
rm *.exons
rm *.3rdpositions
rm *.4fold
rm *.introns_flanking
rm *.synonymous



   
   done